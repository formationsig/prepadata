:warning: Ceci est une version PDF du document à modifier en MARKDOWN UNIQUEMENT :warning:



#   ![mod1_clean][001] module socle 0 - Les données archéologiques : préparer et consolider ses tableaux

**objectifs: ** 

* Préparer et consolider des données archéologiques dans un tableau élémentaire en vue d'un futur traitement statistique .
* Utiliser un tableur pour nettoyer, consulter et manipuler des données (depuis le tri simple jusqu'au tableau croisé dynamique)
* Acquérir le vocabulaire simple de description et de caractérisation d'un tableau de données. 

**logiciels utilisés:** tableur ([![logo_libro][002] Libre Office Calc](https://fr.libreoffice.org/))

**durée**: 1 a 2 jours

**prérequis:** aucun ! ce module est destiné à tous les agents. Il est en revanche un prérequis pour toutes les formations SIG et Stat.



# Introduction

Ce premier module est destiné a la préparation de données archéologiques sous la forme de tableaux élémentaires ainsi qu'au nettoyage et la consolidation des données afin de pouvoir les manipuler dans le cadre d'une étude archéologique avec un tableur, un logiciel de statistique ou de SIG.

Cette étape peut être faite avec un tableur en utilisant les fonctions courantes (depuis le tri jusqu'au Tableau Croisé en passant par les fonctions).



# Pourquoi ranger sa chambre ?


| DONNÉES              | TRIER |
| :---------: | :-------: |
| ![lego_data][003] | ![data_sort][004] |
| **ORGANISER / CONSOLIDER** | **REPRÉSENTER** |
| ![lego_arrange][005] | ![lego_graph][006] |

@datavizdiva



--------------------------------------

### Les encarts

Dans ce déroulé vous trouverez 4 types d'encarts:

{% hint style='info' %}
Information: indique une information d'ordre général
{% endhint %}

{% hint style='danger' %}
Attention: indique une information importante ou un problème récurent
{% endhint %}

{% hint style='tip' %}
Astuce: indique une astuce, un raccourci
{% endhint %}

{% hint style='working' %}
Exercice: indique des manipulations à faire ou l'énoncé d'un exercice
{% endhint %}

{% hint style='zob' %}
Autre: exemples, aparté, information facultative
{% endhint %}

---------------------

# 1. Utilisation du tableur

 {% hint style='working' %}

* Faire une copie des données dans **un nouveau dossier [travail]** 
* Ouvrir **LibreOffice Calc** puis le fichier [**travail]/data.ods**

{% endhint %}

 {% hint style='danger' %}

Les ordinateurs sous Windows on tendance à vouloir ouvrir tous les formats de tableaux avec MS Excel :disappointed:

Pour forcer l'ouverture d'un fichier avec LibreOffice Clac il est préférable de faire un **clic droit → Ouvrir avec… →  LibreOfficeCalc.** 

{% endhint %}



## 1.1 Présentation générale

Nous avons fait le choix d'utiliser le logiciel [![logo_calc][002]**LibreOffice Calc**](https://fr.libreoffice.org/), Pour plusieurs raisons:

* Il est installé par défaut sur tous les ordinateurs Inrap.

* C'est un logiciel libre (tout le monde peut  l'installer gratuitement) et multi plateforme (Windows, Mac OS, Linux, etc).
* Il dispose de toutes les fonctions nécessaire à la préparation des données, à leur manipulation et à leur représentation graphique.
*  Il possède les mêmes fonctionnalités qu'Excel et son interface est minimaliste est proche des anciennes versions de celui-ci.
* il peut importer et exporter tous les formats de fichiers courants (csv, dbf, xls, xlsx), et le format ods est compatible avec les autres tableurs.



### L'interface

![calc_gui][007]

Un **classeur**, c'est le fichier qui s'ouvre automatiquement au lancement de **Calc**. Le nom par défaut de ce classeur est *Sans nom 1*. 

Chaque classeur est composé d'une ou plusieurs **feuilles de calcul**. Les feuilles de calcul qui constituent ce classeur portent également un nom par défaut (*Feuille1, Feuille2 ….*) et on peut y accéder par un système d'onglets.

Une feuille de calcul est constituée **de lignes et de colonnes**. 

Ces lignes et ces colonnes délimitent des **cellules**. La cellule est l’unité de base de la feuille de calcul.

 {% hint style='info' %}
Quel est le nombre maximum de cellules dans une feuille de calcul LibreOffice  ?

- Le nombre maximum de colonnes est de 1024 (de la colonne A à la colonne AMJ) ;
- le nombre maximum de lignes est de 1 048 576 ;
- le nombre maximum de cellules dans une feuille est de 1 073 741 824 soit plus de 1 milliard de cellules) ;
- le nombre maximum de feuilles dans un classeur est 10000.
{% endhint %}

On se déplace dans la feuille grâce aux ascenseurs horizontaux et verticaux et l'on peut zoomer/dézoomer grâce au curseur en bas à droite.

### Les Menus

{% hint style='tip' %}
Un menu s’ouvre par un clic sur son nom ou par [Alt] + la lettre souligné (*ex: [Alt] + F pour le menu Fichier*)
{% endhint %}

{% hint style='tip' %}
Les outils les plus courants disponibles dans les menus sont aussi accessibles:

* via des icônes dédiés dans la Barre d'outils

* via des raccourcis → appui simultané sur la touche [Ctrl] + une lettre (ou parfois [Ctrl] + :arrow_up: MAJ + une lettre)

  **Note: les raccourcis ont indiqués ci-dessous, il faut les executer en appuyant simultanéement sur la touche [Ctrl] ↓**

  {% endhint %}

  

* **Menu Fichier :** gestion des documents (création, ouverture, enregistrement, impression,... )

  Nouveau [N], Ouvrir  [O], Enregistrer [S], Enregistrer sous :arrow_up: + [S], Aperçu :arrow_up: + [O], Imprimer  [P]

* **Menu Edition :** sélectionner des cellules et éditer leur contenu
  Annuler [Z], Couper [X], Copier [C], Coller [V], **Collage spécial**, **Rechercher** [F], **Remplacer** [H], etc         

* **Menu Affichage :** paramétrage de l'interface  (Barres d'outils, Zoom, etc) et de l'affichage de la feuille (**Scinder la fenêtre** et **Fixer Lignes et Colonnes**) 

* **Menu Insertion :** insertion d'une Formule [F2] et **création de graphiques**
  
* **Menu Format :** gestion des formats d'affichages divers et des cellules

   Fusionner les cellules, (Format de ) Cellules [1]

* **Menu Styles:** on oublie ! rien d'intéressant pour la préparation de données

* **Menu Feuille:** Insertion / Suppression de Lignes, Colonnes, Feuilles

* **Menu Données:**  :heart: permet d’utiliser un certain nombre de commandes destinées au **traitement de données depuis les tris jusqu'au tableau Croisé Dynamique** 

   **Trier**,  **AutoFiltre**,  **Table dynamique** )

* **Menu Outils :** on oublie ! rien d'intéressant pour la préparation de données

* **Menu Fenêtre:** sert essentiellement à ouvrir plusieurs fenêtres (classeurs)

* **Menu Aide**:  Aide [F1] et  A propos de LibreOffice (Vérification de la version du logiciel)



## 1.2 Manipulations simples  du tableur

### Les formes du pointeur de la souris

| Forme du pointeur | Emplacement dans la fenêtre | Action avec la souris |
| :---------------: | --------------------------- | :-------------------- |
|  ![pointeur ligne col][009]  | A la jonction entre 2 numéros de lignes ou de 2 entêtes de colonnes | **double clic:** optimise la largeur de la colonne (ou la hauteur de la ligne) selon le contenu. **par glissé-déposé** *idem* mais ajustement manuel. **Note: Si sélection préalable de plusieurs lignes/colonnes** l'ajustement (auto ou manuel) s'applique à l'ensemble de la sélection. **Clic droit:** donne accès à des outils d'édition (copie, coller,..., format de cellule, Fixer ligne ou colonne) |
|        ![pointeur blanc][008]           | Partout dans  la feuille de calcul | **Simple clic:** Sélectionner une cellule ou un ensemble de cellule en glissé-déposé. Une fois la selection faite, celle-ci peut être déplacée par un nouveau glissé-déposé. **Double clic dans une cellule: ** rend la cellule éditable. |
|        ![pointeur texte][011]        | Sur une cellule éditable | Aucune: indique que la cellule est éditable |
|  ![pointeur croix][010]  | Dans le coin inférieur droit d'une cellule (ou un ensemble de cellule) après l'avoir sélectionnée | **glissé-déposé vers le bas ou la droite OU double-clic:** incrémente les cellules suivantes de façon croissante. **glissé-déposé vers le haut ou la gauche:** incrémente les cellules suivantes de façon décroissante. |

### Sélectionner des cellules

| Action / Raccourci |Description|
| ---- |---|
|   **Clic sur une cellule**   | Sélectionner de la cellule|
|   **Clic sur une entête de colonne / ligne**   | Sélectionner toute la colonne / ligne|
|   **Glissé-déposé sur un ensemble de cellules**   | Sélectionner un ensemble cellule|
|   **Clic sur une cellule puis [Alt] + clic sur une autre cellule**   | Sélectionner de toute les cellules entre les 2 cellules cliquées cellule|
|   **Clic sur une cellule puis [Ctrl] + clic sur une autre cellule** *(+ répeter [Ctrl] + clic )*   | Sélectionner les cellules cliquées (même discontinues) |
|   **[Ctrl] + [A]** (ou clic sur la jonction des entêtes de ligne et colonnes)   | Sélectionner toute la feuille (y compris les cellules vides|
|   **[Ctrl] + [\*]**   | Sélectionner la plage de données (s'arrête à la première ligne & colonne vide)|

### Copier-Coller

**![coll_spe][101] couper/![coll_spe][103] copier→![coll_spe][102]        coller**:
Pour faire un copier coller: **[Ctrl] + [C]** de la cellule ou zone qui vous intéresse et **[Ctrl] + [V]** à l'emplacement ou vous souhaitez coller.
{% hint style='zob' %}
Ici c’est la valeur de la cellule qui est copiée/ collée, dans le cas d’une cellule avec formule, c’est la formule qui est collée puis réactualisée.
{% endhint %}

{% hint style='working' %}
démonstrations + exercice 1.2.1
{% endhint %}

![coll_spe][100] **collage spécial  [Ctrl] + :arrow_up: + [V]**:
Pour coller la valeur plutôt que la formule,  il est nécessaire de faire un copié-collage spécial.
Dans la cellule ou vous voulez coller les valeurs, faire un **clic-droit → ![coll_spe][100] collage spécial → ![coll_spe][100] collage spécial** ou juste  **[Ctrl] + :arrow_up: + [V]**
Pour ne coller que les valeurs, décocher la case :white_large_button: Formules.

 ![ui coll spe][012]

{% hint style='tip' %}
On peut se servir du collage spécial pour  :negative_squared_cross_mark:**Transposer** son tableau c'est à dire inverser lignes et colonnes 
{% endhint %}

{% hint style='working' %}
Couper, Copier, Coller, Collage spécial → Valeurs, Collage spécial transposer: démonstrations + exercice 1.2.2
{% endhint %}

### Rechercher-Remplacer

![coll_spe][100] **Rechercher [Ctrl] + [F]**:
Pour faire une recherche dans la feuille, faire un **[Ctrl] + [F]** → une barre de recherche s'ouvre en bas de la fenêtre → taper la recherche puis [Tout rechercher] 

{% hint style='info' %}
[Ctrl] + [F] avec F pour *Find* est un raccourci commun présents dans la plupart des  logiciels (y compris les  explorateurs internet, les lecteurs PDF,...)
{% endhint %}

**Rechercher - Remplacer [Ctrl] + [H]**:
Pour ouvrir une boîte de dialogue **Rechercher et remplacer** utiliser le raccourci **[Ctrl] + [H]**

{% hint style='working' %}
Rechercher/Remplacer: exercices 1.2.3
{% endhint %}

## 1.3 Formules et fonctions

### Principes

**Une formule ? :**

* C'est un série de valeurs, de références de cellules, de noms, de fonctions ou d’opérateurs qui est contenue dans une cellule et **qui génère une nouvelle valeur** à partir des valeurs existantes. 
* :warning: Une formule **commence toujours par un signe égal `=`**
* Elle **permet de réaliser des opérations**, telles que des additions, des multiplications **et des comparaisons** de valeurs figurant dans une feuille de calcul.

{% hint style='zob' %}
exemple: `= MOYENNE(SOMME(A1:B3);SOMME(1;2;3;4))`
{% endhint %}

**Saisir une formule :**

* **Dans la barre de formules**  ![barre formule][013]
  * cliquer sur une cellule
  * dans la barre de formule à droite, commencer par le signe **=**
  * taper la formule ou cliquer sur l'icône **Assistant Fonctions** ![fonction][105]
  * Valider en cliquant sur l'icône de validation ![icon validation][106] (ou avec [Entrée] :leftwards_arrow_with_hook: )
* **Directement dans la cellule**:
  * cliquer sur une cellule
  * commencer par le signe **=**
  *  taper la formule 
  * valider par [Entrée] :leftwards_arrow_with_hook: 

### Les opérateurs

Une formule mêle généralement des valeurs et des adresses de cellules et des opérateurs. Les opérateurs sont de 3 types:

* Les opérateurs arithmétiques qui permettent d’effectuer des opérations mathématiques de base. Ils combinent des valeurs numériques et génèrent des résultats numériques.
* Les opérateurs de comparaison permettent de comparer deux valeurs et génèrent la valeur logique VRAI ou FAUX.
* Les opérateurs de texte permettent de lier plusieurs chaînes de caractères en une seule chaîne de caractères combinée.

#### Les opérateurs arithmétiques

| Opérateur | Fonction | exemple |
| :---------------: | -------- | ----------------- |
| **+**    | Addition| `=1+2`|
| **-**   | Soustraction (ou négation, s'il est placé devant une valeur)| `=5-3` |
| **/** | Division | `=10/2` |
| **\***    | Multiplication | `=4*3` |
| **^** | Exposant | `=2^3` |

* ils peuvent être imbriqués: ` = 5+2*B3` ou ` B3=2`
* ils respectent les priorités mathématiques:
* les multiplications & les divisons ont le même niveau de priorité, si plusieurs opérateurs les opérations sont faites de gauche à droite.
  * les multiplications & les divisons sont prioritaires par rapport aux additions & les soustractions.
  * les additions & les soustractions  ont le même niveau de priorité, si plusieurs opérateurs les opérations sont faites de gauche à droite.
* Vous pouvez préciser l'ordre de calcul en posant des parenthèses.

{% hint style='zob' %}
exemple: 
`=5+2*B3`aura pour résultat  9 (la multiplication se fait en premier)
`= (5+2)*B3` aura pour résultat 14 (les parenthèses indiquent que l'addition se fait en premier et que c'est le résultat de l'addition qui est multiplié par B3).
{% endhint %}

{% hint style='working' %}
Opérateurs arithmétiques: exercices
{% endhint %}

#### Les opérateurs de texte
| Opérateur | Fonction | exemple |
| :---------------: | ----------- | -------------- |
| **&**    | **[Concatène](https://fr.wikipedia.org/wiki/Concat%C3%A9nation)** deux valeurs pour générer une chaîne de caractères continue c'est à dire qu'il permet de mettre bout à bout au moins deux chaînes de caractères. | `=A1&B1` ou `=CONCATENER(A1;B1)` |

{% hint style='zob' %}
exemple: 
`="Archéologie" & "/" & "Préventive"`retournera le texte suivant:  `Archéologie/Préventive`
{% endhint %}

On peut aussi utiliser la fonction `=CONCATENER( texte1; texte2; ...)`

{% hint style='info' %}
* Pour ajouter du texte dans la formule (et non pas une cellule) il suffit de le mettre entre “guillemet américains” 
* L'usage des espaces entre les opérateurs est recommandée pour la lisibilité mais elle n'est pas obligatoire
{% endhint %}

{% hint style='tip' %}
* L'usage des espaces entre les opérateurs est recommandée pour la lisibilité mais elle n'est pas obligatoire
* En revanche il peut être utile d'insérer des espaces entre les " guillemets " 
{% endhint %}


#### Les opérateurs de comparaison

Les opérateurs de comparaison retournent une réponse soit vraie soit fausse.
On peut obtenir une réponse directe VRAI ou FAUX  en saisissant un formule comme `=A1>A2`: si le contenu de la cellule A1 est supérieur à celui de la cellule A2 la réponse VRAI est retournée, et dans le cas contraire laréponse FAUX est retournée.

Les opérateurs de comparaison se rencontrentsurtout dans des formules qui utilisent la fonction  **SI** et retournent une réponse soit vraie soit fausse.

| Opérateur | Fonction            | exemple  |
| :-------: | ------------------- | -------- |
|   **=**   | Égal                | `A1=B1`  |
|   **>**   | Supérieur à         | `A1>B1`  |
|   **<**   | Inférieur à         | `A1<B1`  |
|  **>=**   | Supérieur ou égal à | `A1>=B1` |
|  **<=**   | Inférieur ou égal à | `A1<=B1` |
|  **<>**   | Différent           | `A1<>B1` |

#### Les opérateurs logiques: les fonctions ET / OU / SI

| Opérateur | Fonction            | exemple  |
| :-------: | ------------------- | -------- |
|   **ET**   | Retourne VRAI **si TOUS les arguments sont VRAI**, sinon renvoie FAUX                    | `=ET(B1>A1;B2="truelle")` |
|   **OU**   | Retourne VRAI si **UNE des conditions est VRAI**, sinon retourne FAUX         | `=OU(B1>A1;B2="truelle")` |
|   **SI**   | Spécifie un test logique à effectuer| `=SI(ET(B1>A1;B2="truelle";"BIEN"; "PAS BIEN")`  |

**La fonction ET**
{% hint style='zob' %}
`=ET(B1>A1;B2="truelle")` retourne VRAI si le contenu de la cellule et supérieure au contenu de la cellule A1 **ET** que le contenu de la cellule B2 est "truelle"
{% endhint %}

**La fonction OU**
{% hint style='zob' %}
`=OU(B1>A1;B2="truelle")` retourne VRAI si le contenu de la cellule et supérieure au contenu de la cellule A1 **OU** que le contenu de la cellule B2 est "truelle"
{% endhint %}

**La fonction SI**
La fonction **SI** effectue un test logique qui va retourner 2 valeurs (**et uniquement 2**) : VRAI ou FAUX 

{% hint style='tip' %}
le resultat peut être  1 et 0 si la cellule est formatée en nombre
{% endhint %}

La fonction SI se décompose en trois parties:
* Le test   
* Le résultat si le test est vrai
* Le résultat si le test est faux
La formule s’écrit `=SI( TEST; Si test est VRAI; Si test est FAUX)`

{% hint style='zob' %}
`=SI(ET(B1>A1;B2="truelle";"BIEN"; "PAS BIEN")` retourne BIEN si le résultat de la fonction test `ET(B1>A1;B2="truelle"`est VRAI sinon elle retournera PAS BIEN
{% endhint %}

{% hint style='info' %}
Il est possible d'imbriquer des fonctions **SI** en l'écrivant de cette façon: `=SI( test1; Si test est VRAI; SI( test2; SI test2 est VRAI; SI( Test1; Si test est VRAI)))`
→ on fait ici un premier test, si le résultat est FAUX alors on en fait un deuxième, etc...
{% endhint %}

### Les principales fonctions

Libreoffice Calc contient plus de 350 fonction intégrées c'est pourquoi il est parfois utile d'utiliser **Assistant Fonctions** ![fonction][105]

Voici les fonctions les plus courantes:

{% hint style='info' %}
Remarque: les noms des fonctions s'écrivent en français
{% endhint %}

| Formule           | Commentaire                                         |
| ----------------- | --------------------------------------------------- |
| **=abs()**        | Calcule la Valeur absolue                           |
| **=racine()**     | Calcule la Racine carrée                            |
| **=somme()**      | Calcule la Somme                                    |
| **=max()**        | Calcule la valeur maximum                           |
| **=min()**        | Calcule la valeur minimum                           |
| **=mediane()**    | Calcule la médiane                                  |
| **=moyenne()**    | Calcule la moyenne                                  |
| **=aujourdhui()** | Retourne la Date du jour                            |
| **=estvide()**    | test si la cellule est vide (VRAI / FAUX)           |
| **=esttexte()**   | test si la cellule contient du texte (VRAI / FAUX)  |
| **=estnum()**     | test si la cellule contient un nombre (VRAI / FAUX) |

{% hint style='danger' %}

Rappel:

* une fonction **commence toujours par le signe `=`**
* une fonction contient **des arguments** qui peuvent être:
  * des nombres `999`
  * du texte `"truelle"`
  * l'adresse d'une cellule `A1` ou d'une plage de cellule (un ensemble continu de cellules) `A1:A10`
  * d'autres fonctions imbriquées ( :warning: dans ce cas elles ne sont pas précédées du signe `=` )
* les arguments d'une fonction sont **séparés par un point-virgule `;`**
* le résultat d'une fonction est soit une valeur (un nombre, une chaîne de caractère ou une date) soit un opérateur booléen (VRAI/FAUX ou 1/0)

{% endhint %}

## 1.4 Adresse et référence d'une cellule

### Adresse d'une cellule

**L'adresse d'une cellule c'est son emplacement dans le tableau**. Une cellule individuelle est identifiée par l'identifiant de sa colonne (lettre), situé en haut des colonnes, et par l'identifiant de sa ligne (nombre), situé du côté gauche du classeur. 
L'adresse d'une cellule permet d'y faire référence et d'utiliser son contenu dans une fonction.

### Référence à un ensemble de cellules

Il est possible de faire référence à un ensembles de cellule:
* avec le séparateur point-virgule **`;`** pour faire référence à plusieurs cellules non contiguës

{% hint style='zob' %}

exemple: `=SOMME(A1;B2;Z3)` retournera la somme des valeurs des 3 cellules

{% endhint %}

{% hint style='tip' %}

Il est aussi possible de sélectionner ces cellules avec plusieurs clic de souris tout en appuyant sur la touche [ctrl]

{% endhint %}

* avec le séparateur deux-points **`:`** pour faire référence à **une plage de cellules** contiguës

{% hint style='zob' %}

exemple:

* `=SOMME(A1:A151)` retournera la somme des 150 valeurs des cellules de la premières colonnes allant de la cellule `A1` à `A151`
* `=SOMME(A1:B3)` retournera la somme des 6 valeurs des cellules de la plage rectangulaire constitué avec la cellule  `A1` en haut à gauche et  `B3` en bas à droite

{% endhint %}

{% hint style='tip' %}

Il est aussi possible de sélectionner ces cellules avec:

* un glissé-déposé depuis la cellule de départ jusqu'à la cellule d'arrivée
* un clic de souris sur la cellule de départ puis tout en appuyant sur la touche [↑] un clic sur la cellule d'arrivée

{% endhint %}

### Adresse absolue et relative

Une cellule individuelle est identifiée par l'identifiant de sa colonne (lettre), situé en haut des colonnes, et par l'identifiant de sa ligne (nombre):

* Une **adresse relative** se notera simplement par une lettre et un chiffre indépendamment de la cellule active, c'est à dire  que si l'on déroule une fonction qui fait référence à cette cellule, la référence s'incrémentera automatiquement.

![ad_relative][107]

* Pour rendre l'**adresse absolue**, lettres et/ou chiffres peuvent être précédés du caractère **\$**.

{% hint style='zob' %}

exemple:

* `B$2` bloquera la référence à la ligne (même si on déroule une fonction vers le bas).

![ad_abs][108]

* `$C5` bloquera la référence à la colonne (même si on déroule une fonction vers la droite).
* `$A$1` bloquera la référence à la ligne et à la colonne.

{% endhint %}

{% hint style='tip' %}
**Exemple utilisation courante: la recherche de doublons avec la fonction `NB.SI`**

exemple `=NB.SI($A$2:$A$500;A2)`

`=NB.SI` est une fonction qui compte les arguments qui correspondent à un critère avec,

* `$A$2:$A$65635` la plage de cellules dans laquelle la recherche va être faites, c'est à dire depuis la cellule `A2` jusqu’à la dernière de la cellule `A65635` → *En déroulant la formule la plage (ou un copié/collé) ne changera pas celle-ci grâce aux **$***

* `A2`  le critère, ici le contenu dans la cellule `A2` dont on cherche le nombre d'occurrences dans la plage définie → *Ici, par contre, les coordonnées de la cellule qui contient le critère varient à chaque ligne et seront incrémentées directement lors de la copie en série)*

En gros la formule dit: « regarde le contenu de la cellule `A2` et comptes combien tu en trouves dans la colonne » : ici ↓ il n’y a pas de doublon du nombre1 , donc il écrit 1 , par contre le nombre 3 y est 2 fois.

![nbsi][109]

{% endhint %}

{% hint style='working' %}
`NB.SI` et adressage des cellules → démonstration puis exercices (dataRTP)
{% endhint %}

## 1.5 Manipuler les données

### Les tris

*Calc* permet de trier les colonnes d'un tableau de données.

Pour cela il suffit, au choix:

*  d’utiliser la commande **Menu Données** → **Trier...** pour modifier la disposition des lignes d’une liste en fonction du contenu de certaines colonnes.

* d'utiliser les icones de tri ascendant/croissant ![tri1][110], de tri descendant/décroissant ![tri1][111] ou de tri multi-colonnes ![tri1][112] (3 colonnes max).

  

{% hint style='working' %}
tris → Réaliser la démo-exo sur la feuille [data_SE]

1. Trier les faits par le nombre de fragments céramiques qu’il contiennent (tri ascendant).

2. Trier les faits par type et diamètre (tris ascendants).

{% endhint %}



### Les filtres

Pour rechercher des données dans une liste, il est possible d'appliquer des filtres, c'est à dire de ne montrer que les sonnées qui répondent à un critère (ou plusieurs). Les filtres sont accessibles depuis **Menu Données** → **Filtres**

L'**Autofiltre** (icône ![autofiltre][113]) permet d'activer les filtres sur toute la première ligne / les entêtes de colonnes:

* une petite flèche noire :arrow_down_small: apparait en bas à droite de chaque entête et applique directement des flèches (type "liste déroulante") aux entêtes de colonne.

![filtre][114]

*  Une fois déroulé il permet de sélectionner l’élément à afficher. Lorsque un élément est sélectionné dans la liste déroulante, *Calc* masque temporairement les lignes ne contenant pas cet élément.

{% hint style='info' %}

Une fois un filtre activé la petite flèche devient bleue ![fleche_bleue][115]

{% endhint %}



Le **Filtre standard** ouvre une fenêtre de requête de type “Nom_de_champ”, “Condition”, “Valeur”. Les filtres peuvent être associés à l’aide des opérateurs booléens `ET` et `OU`.

{% hint style='working' %}
filtres → Réaliser la démo-exo sur la feuille [data_RTP]

1. Autofiltre: Filtrer les types de faits

2. Filtre standard: Trier les faits par type et diamètre (tris ascendants).

{% endhint %}

{% hint style='info' %}

Pour désactiver le filtre (ré-afficher toutes les données) :

- Soit cliquer sur le filtre actif et choisir **Tout**
- Soit désactiver l’Autofiltre: **Menu Données** → ![autofiltre][113]**Autofiltre**

{% endhint %}

{% hint style='info' %}

Pour compter le nombre d’individus (de lignes) il suffit de sélectionner une colonne puis de regarder en Bas à droite: `Compter =` (Pour choisir la fonction affichée à cet endroit: clic droit → choisir **Nombre**). 

![compter][116]

{% endhint %}

{% hint style='working' %}
filtres → Réaliser la démo-exo sur la feuille [data_SE]
Déterminer à l'aide des filtres :
- Le nombre de TP avec une forme ovale en plan
- Le nombre de silo avec une forme en plan circulaire et un diamètre supérieur à 1.2 m Filtre silo + circulaire puis TRI décroissant sur diamètre

{% endhint %}



{% hint style='tip' %}
Le filtre peut être une manière de faire une requête sur son tableau élémentaire. 

Pour enregistrer le fruit de cette requête il est possible de créer une nouvelle colonne est d'inscrire la valeur 1 pour les lignes filtrées.

{% endhint %}



### Le tableau croisé (ou TCD)

Un **tableau croisé dynamique** ou TCD, est une fonctionnalité des tableurs qui permet de générer une synthèse d'une table de données brutes. 

Le TCD permet de regrouper/d'aggréger des données selon une ou plusieurs colonnes et faire les opérations nécessaires entre les montants correspondants (sommes, moyennes, comptages, *etc*).

Le TCD peut donc créer des mises en forme de tableaux en répartissant les différents champs voulus, en abscisses ou en ordonnées.

Il permet de composer rapidement un tableau synthèse provenant d'une masse de données.



**Un tableau croisé dynamique permet donc d’analyser et de synthétiser les données.**

Il prend comme en-tête de ligne et de colonne les données extraites du tableau d’origine (généralement un tableau élémentaire c’est à dire avec des individus en lignes et des variables en colonnes) et effectue un calcul de synthèse au croisement des lignes et colonnes correspondantes.



Son aspect dynamique tient au fait que l’on peut actualiser les tableaux croisés si les données du tableau source ont été modifiées.

{% hint style='danger' %}
La création d’un tableau croisé dynamique est une manière efficace de synthétiser le contenu d'une ou plusieures colonnes. Il faut néanmoins respecter quelques règles:

* Chaque colonne doit contenir une **entête**, qui correspondra à un nom de champ dans le tableau croisé dynamique.

* Dans une colonne, le contenu des cellules doivent être de même nature. 

* Dans une colonne,  les cellules doivent contenir des valeurs qui se répètent.

* :warning: Les colonnes ne doivent contenir ni filtre, ni sous-totaux.

{% endhint %}

  

Pour créer un tableau croisé dynamique avec LibreOffice Calc, il faut:

1. Sélectionner:
   * soit une colonne (clic sur la lettre au dessus de l'entête)
   *  soit l'ensemble des données du tableau (avec le raccourci  **[Ctrl]+[\*]**) 
   * ou une partie du tableau élémentaire que l’on veut synthétiser (avec un glissé-déposé ou clic de la cellules en haut à gauche → :arrow_up: enfoncé → clic sur la cellule en bas à droite)
   
   ![tcd_selection][118]
   
2. **Menu Données → ![tcd][117]Tableau dynamique → Insérer ou éditer...**

3. Dans la boîte de dialogue laisser l’option Sélectionner la source :o: Sélection active → :ok:

4. Dans la boîte de dialogue **Mise en page de la table dynamique**:
   
   ![tcd_mep][119]
   
   1. Faire un glissé-déposé d'une entête de colonne (correspondant à la colonne que vous voulez mettre en colonne dans le tableau de synthèse)  depuis le panneau **Champs disponibles**: vers le panneau **Champs de colonne:**
   
   2. Faire un glissé-déposé d'une entête de colonne (correspondant à la colonne que vous voulez mettre en ligne dans le tableau de synthèse)  depuis le panneau **Champs disponibles**: vers le panneau **Champs de ligne:**
   
   3. Faire un glissé-déposé d'une entête de colonne (correspondant à la colonne contenant les données que vous voulez synthétiser selon les critères désignés en ligne et en colonne)  depuis le panneau **Champs disponibles**: vers le panneau **Champs de données:**
   
   4. Dans le panneau **Champs de données:**, double cliquer sur la fonction affichée pour choisir la fonction mathématique à appliquer pour faire le résumé des données selon les critères désignés en ligne et en colonne dans le tableau de synthèse.
         ![tcd_choix_fonciton][120]
   5. :ok: → :ok: → admirer le résultat affiché dans une nouvelle feuille intitulée *Table dynamique_FeuilleX* :smile:
   
   
   
   ![tcd_resultat][121]



{% hint style='info' %}

Un clic droit dans le TCD permet, entre autres:

* ![tcd][122] **Propriétés**: de revenir aux propriétés / mise en page du tableau.
* ![tcd][123] **Actualiser**: d'actualiser / regénérer le tableau (si les données sources ont été modifiées)'.

* **Filtrer...**: de filtrer les données sources.

{% endhint %}

{% hint style='tip' %}

Pour **"fixer"**  le TCD, il suffit de le sélectionner et de faire un copié-coller.

{% endhint %}

{% hint style='working' %}

TCD → Exercices avec la feuille [data_SE]
- Calculer la somme des fragments de céramique par type de faits et par période.
- Calculer le diamètre moyen des types de faits par période.
- .Calculer la profondeur moyenne des silos de l'antiquité.

{% endhint %}



### ![GroupStat][124] Group stat: le TCD dans ![qgis][125]

cf. extraits de la formation ![sig32][126] SIG 3.2 Analyses spatiales:

* la [**recherche de doublons avec l'extension Group Stat**](https://formationsig.gitlab.io/sig32/pas_a_pas/SIG32_Analyses_Spatiales.html#1331-recherche-de-doublons-avec-lextension-group-stat) 

* l'[**aggrégation de la somme de céramiques par sondage**](https://formationsig.gitlab.io/sig32/pas_a_pas/SIG32_Analyses_Spatiales.html?q=#%C3%A9tape-1-pr%C3%A9parer-les-donn%C3%A9es-tabul%C3%A9es)

  


[001]:images/mod1_clean.png	"logo formation - module 1"
[002]:images/logo_libro32.png	"logo LibreOffice Calc"
[003]:images/lego_data.png	"données"
[004]:images/lego_sort.png	"trier"
[005]:images/lego_arrange.png	"organiser"
[006]:images/lego_graph.png	"représenter"
[007]:images/calc_gui.png	"interface de LibrO Calc"
[008]:images/pointeur_blanc.png	"pointeur de souris blanc"
[009]:images/pointeur_col.png	"pointeur de souris pour agrandir lignes et colonnes"
[010]:images/pointeur_croix.png	"pointeur de souris pour dérouler"
[011]:images/pointeur_texte.png	"pointeur de souris pour écrire du texte dans une cellule"
[012]:images/ui_collage_special.png	"boite de dialog collage special"
[013]:images/barre_formule.png	"barre de formule"

[100]:images/icon_collage_special.png
[101]:images/icon_couper.png
[102]:images/icon_coller.png
[103]:images/icon_copier.png
[104]:images/icon_rechercher.png
[105]:images/icon_fonction.png
[106]:images/icon_validation.png
[107]:images/calc_adresse_relative.png "calc imprim adresse relative"
[108]:images/calc_adresse_absolue.png "calc imprim adresse absolue"
[109]:images/calc_nbsi.png "calc imprim nb.si"
[110]:images/icon_tri_asc.png "icon tri croissant"
[111]:images/icon_tri_desc.png "icon tri decroissant"
[112]:images/icon_tri.png "icon tri standard"
[113]:images/icon_autofiltre.png "icon autofiltre"
[114]:images/filtre.png "calc imprim filtre"
[115]:images/filtre_fleche_bleu.jpg "icon fleche bleue"
[116]:images/calc_compter.png "calc imprim compter"
[117]:images/icon_tcd.png "icon tableau dynamique"
[118]:images/calc_tcd_selection.png  "calc imprim tcd selection"
[119]:images/calc_tcd_mep.png  "calc imprim tcd mise en page"
[120]:images/calc_tcd_mep2.png  "calc imprim tcd choix fonction"
[121]:images/calc_tcd_resultat.png  "calc imprim tcd resultat"
[122]:images/icon_tcd_proprietes.png  "calc imprim tcd proprietes"
[123]:images/icon_tcd_actu.png  "calc imprim tcd actualiser"
[124]:images/iconGS.png  "icon group stat"
[125]:images/qgis24.png  "icon QGIS"
[126]:images/sig32_24.png  "icon formationSIG 3.2"